package rainbows

// RequestHeaders is used to define a manager for request headers.
type RequestHeaders interface {
	// Get is used to get a requests headers as bytes.
	// Note that the bytes should not be used after the request is done.
	Get(Key string) []byte

	// GetString is used to get a requests headers as a string.
	GetString(Key string) string

	// Set is used to set the headers in the request. Note this does not edit the response.
	// This is mainly designed to be used in middleware.
	Set(Key, Value string)

	// Keys is used to get the keys of headers.
	Keys() []string
}

// Request is used to define a request manager.
type Request interface {
	// Headers is used to get a interface which can be used to manage the request headers.
	Headers() RequestHeaders

	// Bind is used to bind the request body to the interface specified.
	Bind(iface interface{}) error

	// Path is used to get the raw path.
	Path() []byte
}
