module gitea.com/jake/rainbows

go 1.16

require (
	github.com/fasthttp/websocket v1.4.3
	github.com/gabriel-vasile/mimetype v1.1.2
	github.com/go-yaml/yaml v2.1.0+incompatible
	github.com/valyala/fasthttp v1.18.0
)
