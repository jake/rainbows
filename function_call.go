package rainbows

import (
	"errors"
	"reflect"
)

// FunctionCall is used to call the route function.
type FunctionCall struct {
	requestArgs    [][]byte
	handler        reflect.Value
	middleware     []reflect.Value
	additionalArgs []interface{}
	req            Request
}

// Used internally for a bad transform.
var badTransform = errors.New("")

// Used to transform the remainder of the arguments.
func transformArguments(reqArgs [][]byte, args []reflect.Value, type_ reflect.Type) ([]reflect.Value, error) {
	// Get the start index.
	startIndex := len(args)

	// Go through each argument.
	argIndex := 0
	for i := startIndex; i < type_.NumIn(); i++ {
		// Get the argument transformer.
		input := type_.In(i)
		transformer := transformers[input]
		if transformer == nil {
			return nil, ErrNoTransformer
		}

		// Get the argument.
		if argIndex == len(reqArgs) {
			return nil, ErrTooManyArguments
		}
		arg, ok, err := transformer(reqArgs[argIndex])
		if err != nil {
			return nil, err
		}
		if !ok {
			return nil, badTransform
		}
		args = append(args, reflect.ValueOf(arg))

		// Add 1 to the argument index.
		argIndex++
	}

	// Return the arguments with no error.
	return args, nil
}

// Run is used to perform the function call with the additional args specified prepended to the handler.
func (f *FunctionCall) Run(additionalArgs ...interface{}) (*Response, error) {
	// Append to the additional arguments.
	f.additionalArgs = append(f.additionalArgs, additionalArgs...)

	// If there's no middleware, call the handler now.
	if len(f.middleware) == 0 {
		// Get the handler type.
		type_ := f.handler.Type()

		// Check the arguments length is ok.
		additionalArgsLen := len(additionalArgs)
		if additionalArgsLen + 1 > type_.NumIn() {
			return nil, ErrTooManyMiddlewareArguments
		}

		// Sanity check the arguments type.
		args := make([]reflect.Value, additionalArgsLen + 1)
		args[0] = reflect.ValueOf(f.req)
		for i := 1; i < additionalArgsLen + 1; i++ {
			// Get the function input.
			input := type_.In(i)

			// Check the type is correct.
			if input != reflect.TypeOf(additionalArgs[i-1]) {
				return nil, ErrMiddlewareArgTypeMismatch
			}

			// Set the argument.
			args[i] = reflect.ValueOf(additionalArgs[i-1])
		}

		// Get the rest of the arguments and then call the handler.
		args, err := transformArguments(f.requestArgs, args, type_)
		if err != nil {
			return nil, err
		}
		res := f.handler.Call(args)
		err, _ = res[1].Interface().(error) // reflect makes error nil for some reason.
		return res[0].Interface().(*Response), err
	}

	// Grab the first middleware handler and remove it.
	first := f.middleware[0]
	f.middleware = f.middleware[1:]

	// Get/call the middleware function.
	args, err := transformArguments(f.requestArgs, []reflect.Value{
		reflect.ValueOf(f.req),
		reflect.ValueOf(f),
	}, first.Type())
	if err != nil {
		return nil, err
	}
	res := f.handler.Call(args)
	return res[0].Interface().(*Response), res[1].Interface().(error)
}
