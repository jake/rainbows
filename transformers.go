package rainbows

import (
	"reflect"
)

// Transformer is used to define a type used to transform a param argument.
type Transformer func([]byte) (iface interface{}, ok bool, err error)

// Defines the transformers.
var transformers = map[reflect.Type]Transformer{}

// AddTransformer is used to add a transformer into Rainbows.
func AddTransformer(typeOf reflect.Type, transformer Transformer) {
	transformers[typeOf] = transformer
}
