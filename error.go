package rainbows

import "errors"

// ErrUnknownContentType is thrown when Rainbows doesn't know how to handle the content type specified by the client.
var ErrUnknownContentType = errors.New("rainbows doesn't know how to handle the content type specified by the client")

// ErrNotFound is the error sent when the router cannot find a page.
var ErrNotFound = errors.New("route not found")

// ErrTooManyMiddlewareArguments is thrown when middleware generates too many arguments for the function signature.
var ErrTooManyMiddlewareArguments = errors.New("too many arguments for the function signature")

// ErrMiddlewareArgTypeMismatch is thrown when a middleware argument mismatches the type signature in the handler.
var ErrMiddlewareArgTypeMismatch = errors.New("middleware argument mismatches the type signature in the handler")

// ErrNoTransformer is thrown when there is no transformer for the argument type.
var ErrNoTransformer = errors.New("there is no transformer for the argument type specified")

// ErrTooManyArguments is thrown when the function signature has too many request arguments.
var ErrTooManyArguments = errors.New("the function signature has too many request arguments")
