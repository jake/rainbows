package rainbows

import (
	"encoding/json"
	"encoding/xml"
	"github.com/go-yaml/yaml"
	"github.com/valyala/fasthttp"
	"strings"
)

type fasthttpRequestHeaders struct {
	*fasthttp.RequestCtx
}

func (h fasthttpRequestHeaders) Get(Key string) []byte {
	header := h.Request.Header.Peek(Key)
	cpy := make([]byte, len(header))
	copy(cpy, header)
	return cpy
}

func (h fasthttpRequestHeaders) GetString(Key string) string {
	return string(h.Request.Header.Peek(Key))
}

func (h fasthttpRequestHeaders) Set(Key, Value string) {
	h.Request.Header.Add(Key, Value)
}

func (h fasthttpRequestHeaders) Keys() []string {
	keys := make([]string, 0, 1)
	h.Request.Header.VisitAll(func(key, _ []byte) {
		keys = append(keys, string(key))
	})
	return keys
}

type fasthttpRequest struct {
	req *fasthttp.RequestCtx
}

func (r fasthttpRequest) Bind(iface interface{}) error {
	contentType := strings.ToLower(strings.SplitN(r.Headers().GetString("Content-Type"), ";", 2)[0])
	switch contentType {
	case "application/json":
		return json.Unmarshal(r.req.Request.Body(), iface)
	case "application/x-yaml", "text/yaml":
		return yaml.Unmarshal(r.req.Request.Body(), iface)
	case "text/xml", "application/xml":
		return xml.Unmarshal(r.req.Request.Body(), iface)
	default:
		x := binders[contentType]
		if x != nil {
			return x(r.req.Request.Body(), iface)
		}
	}
	return ErrUnknownContentType
}

func (r fasthttpRequest) Headers() RequestHeaders {
	return fasthttpRequestHeaders{r.req}
}

func (r fasthttpRequest) Path() []byte {
	return r.req.Path()
}

func newFasthttpRequest(req *fasthttp.RequestCtx) Request {
	return fasthttpRequest{req}
}
