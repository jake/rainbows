package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

// Defines the supported routes.
var supportedRoutes = []string{
	"GET", "POST", "PATCH", "PUT" , "DELETE",
}

// Parses the methods.
func parseMethods(methodsRaw string) []string {
	s := strings.Split(methodsRaw, ",")
	for i, v := range s {
		v = strings.ToUpper(v)
		s[i] = v
		found := false
		for _, x := range supportedRoutes {
			if x == v {
				found = true
				break
			}
		}
		if !found {
			panic("unsupported method")
		}
	}
	return s
}

// Establishes what paths we should use.
func whichPath(paths []string) string {
	text := "Please select which path you want:\n"
	for i, v := range paths {
		text += fmt.Sprintf("%d) %s\n", i, v)
	}
	_, _ = fmt.Print(text)
	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Print("Enter the index of the path: ")
		input, _ := reader.ReadString('\n')
		x, err := strconv.ParseUint(input, 10, 64)
		if err != nil {
			continue
		}
		val := int(x)
		if val >= len(paths) {
			continue
		}
		return paths[val]
	}
}

// Generates the file fragment used for adding the routes.
func generateRoutesCalls(methods, attrs, types []string) string {
	// Go through and make the args.
	parts := make([]string, len(attrs))
	for i, v := range attrs {
		type_ := types[i]
		parts[i] = v+" "+type_
	}
	args := strings.Join(parts, ", ")
	if args != "" {
		args = ", " + args
	}

	// Generate the parts.
	parts = make([]string, len(methods))
	for i, v := range methods {
		content := `    rainbows.`+v+`(func(req rainbows.Request`+args+`) (*rainbows.Response, error) {
        // TODO: Add content.
        return rainbows.Text("No content has been added to this route yet.", 404)
    })`
		parts[i] = content
	}
	return strings.Join(parts, "\n\n")
}

// Create a project route.
func route(methodsRaw, pathRaw string) {
	// Get the information from the arguments.
	methods := parseMethods(methodsRaw)
	split := strings.Split(pathRaw, "/")
	if split[0] == "" {
		// Reslice chopping off the first string.
		split = split[1:]
	}

	// Go through each file in the main package. Try and find the roots.
	files, err := ioutil.ReadDir(".")
	if err != nil {
		panic(err)
	}
	paths := make([]string, 0)
	for _, v := range files {
		if !v.IsDir() && strings.HasSuffix(v.Name(), ".go") {
			b, err := ioutil.ReadFile(v.Name())
			if err != nil {
				panic(err)
			}
			m, err := rootRegex.FindStringMatch(string(b))
			if err != nil {
				panic(err)
			}
			for m != nil {
				p := m.GroupByNumber(1).String()
				fi, err := os.Stat(p)
				if err != nil {
					panic(err)
				}
				if !fi.IsDir() {
					panic("expected folder for Root")
				}
				paths = append(paths, p)
				m, _ = rootRegex.FindNextMatch(m)
			}
		}
	}

	// Handle no paths.
	if len(paths) == 0 {
		panic("expected at least 1 Root usage")
	}

	// Figure out the appropriate path.
	var p string
	if len(paths) == 1 {
		// Only 1 option.
		p = paths[0]
	} else {
		// Ask the user which path they want.
		p = whichPath(paths)
	}

	// Figure out how the path will be stored.
	attrs := make([]string, 0, len(split))
	types := make([]string, 0, len(split))
	if len(split) == 0 {
		// The file path will be path joined to index.go.
		p = filepath.Join(p, "index.go")
	} else {
		// Follow the path.
		for i, v := range split {
			if strings.ContainsRune(v, ':') {
				// This is a param.
				s := strings.SplitN(v, ":", 2)
				if s[0] == "" {
					// Assume the type to be string.
					s[0] = "string"
				}
				types = append(types, s[0])
				attrs = append(attrs, s[1])
				split[i] = "param_" + s[1]
			}
		}

		// Look through for .go files which conflict.
		for i := 0; i < len(split)-1; i++ {
			// Get the value.
			v := split[i]

			// Check if <path>.go exists or the path.
			p = filepath.Join(p, v)
			info, err := os.Stat(p)
			if err == nil && info.IsDir() {
				continue
			}
			if _, err = os.Stat(p + ".go"); !os.IsNotExist(err) {
				if err := os.Mkdir(p, 0777); err != nil {
					panic(err)
				}
				if err := os.Rename(p + ".go", filepath.Join(p, "index.go")); err != nil {
					panic(err)
				}
				continue
			}

			// Create the directory.
			if err = os.Mkdir(p, 0777); err != nil {
				panic(err)
			}
		}

		// Set the last part of the path.
		p = filepath.Join(p, split[len(split)-1])
		info, err := os.Stat(p)
		if err == nil && info.IsDir() {
			p = filepath.Join(p, "index.go")
		} else {
			if _, err = os.Stat(p + ".go"); os.IsNotExist(err) {
				p += ".go"
			} else {
				if err := os.Mkdir(p, 0777); err != nil {
					panic(err)
				}
				if err := os.Rename(p + ".go", filepath.Join(p, "index.go")); err != nil {
					panic(err)
				}
				p = filepath.Join(p, "index.go")
			}
		}
	}

	// Generate the part of the Go file.
	filePart := generateRoutesCalls(methods, attrs, types)

	// Check if the file exists.
	if b, err := ioutil.ReadFile(p); err == nil {
		// Merge the contents.
		s := strings.Replace(string(b), "func init() {", "func init() {\n" + filePart + "\n", 1)
		if err = ioutil.WriteFile(p, []byte(s), 0666); err != nil {
			panic(err)
		}
	} else {
		// File doesn't exist. Go ahead and generate it.
		fileContents := `package rainbows_route

import "gitea.com/jake/rainbows"

func init() {
` + filePart + `
}
`
		if err := ioutil.WriteFile(p, []byte(fileContents), 0666); err != nil {
			panic(err)
		}
	}

	// Re-run the code generation.
	generate()
}
