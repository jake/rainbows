package transformers

import (
	"gitea.com/jake/rainbows"
	"reflect"
)

func init() {
	rainbows.AddTransformer(reflect.TypeOf(([]byte)(nil)), func(bytes []byte) (iface interface{}, ok bool, err error) {
		cpy := make([]byte, len(bytes))
		copy(cpy, bytes)
		return cpy, true, nil
	})
}
