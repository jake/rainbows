package transformers

import (
	"gitea.com/jake/rainbows"
	"reflect"
)

func init() {
	rainbows.AddTransformer(reflect.TypeOf(""), func(bytes []byte) (iface interface{}, ok bool, err error) {
		return string(bytes), true, nil
	})
}
