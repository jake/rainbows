package transformers

import (
	"gitea.com/jake/rainbows"
	"reflect"
	"strconv"
)

func init() {
	rainbows.AddTransformer(reflect.TypeOf(uint(0)), func(bytes []byte) (iface interface{}, ok bool, err error) {
		x, err := strconv.ParseUint(string(bytes), 10, 64)
		return uint(x), err == nil, nil
	})
}
