package transformers

import (
	"gitea.com/jake/rainbows"
	"reflect"
	"strconv"
)

func init() {
	rainbows.AddTransformer(reflect.TypeOf(0), func(bytes []byte) (iface interface{}, ok bool, err error) {
		x, err := strconv.Atoi(string(bytes))
		return x, err == nil, nil
	})
}
