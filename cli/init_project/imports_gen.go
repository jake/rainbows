package main

// This file has been automatically generated by the Rainbows CLI - DO NOT EDIT.
// To import new routes, run "go generate" with the Rainbows CLI and Go installed.
//go:generate rainbows generate

import (
	_ "packagenamehere/transformers"

	_ "packagenamehere/root/name"
	_ "packagenamehere/root/name/json"
	_ "packagenamehere/root/name/xml"
)
