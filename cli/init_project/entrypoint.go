package main

import (
	"gitea.com/jake/rainbows"
	"os"
)

func main() {
	rainbows.Root("root")
	Host := os.Getenv("HOST")
	if Host == "" {
		Host = "127.0.0.1:8080"
	}
	rainbows.Start(Host)
}
