package name

import "gitea.com/jake/rainbows"

func init() {
	rainbows.GET(func(_ rainbows.Request, name string) (*rainbows.Response, error) {
		return rainbows.Text(name, 200)
	})
}
