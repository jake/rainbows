package json

import "gitea.com/jake/rainbows"

func init() {
	rainbows.GET(func(_ rainbows.Request, name string) (*rainbows.Response, error) {
		return rainbows.JSON(name, 200)
	})
}
