package main

import (
	"fmt"
	"github.com/radovskyb/watcher"
	"os"
	"os/exec"
	"os/signal"
	"strings"
	"sync"
	"syscall"
	"time"
)

// Runs "go generate" and returns any errors.
func goGenerate() (err error) {
	return exec.Command("go", "generate").Run()
}

// Launch a development instance of a Rainbows application.
func dev() {
	// Create the watcher.
	w := watcher.New()
	wd, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	if err = w.AddRecursive(wd); err != nil {
		panic(err)
	}
	w.SetMaxEvents(1)
	go func() {
		if err = w.Start(time.Millisecond * 100); err != nil {
			panic(err)
		}
	}()

	// (Re-)loads the development server.
	devServerMu := sync.Mutex{}
	var devServer *exec.Cmd
	loadDevServer := func() {
		// Generate the content.
		fmt.Println("🔨 Running \"go generate\"")
		if err := goGenerate(); err != nil {
			fmt.Println("💥 Failed to generate content:", err)
			return
		}

		// (Re-)loads the development server.
		devServerMu.Lock()
		if devServer != nil {
			_ = devServer.Process.Signal(syscall.SIGINT)
		}
		fmt.Println("🚀 Running \"go build -o bin && ./bin\"")
		devServer = exec.Command("go", "build", "-o", "bin")
		devServer.Stderr = os.Stderr
		devServer.Stdout = os.Stdout
		if err := devServer.Run(); err != nil {
			fmt.Println("💥 Application failed to build:", err)
			return
		}
		devServer = exec.Command("./bin")
		devServer.Stderr = os.Stderr
		devServer.Stdout = os.Stdout
		go func(c *exec.Cmd) {
			if err := c.Run(); err != nil {
				x, ok := err.(*exec.ExitError)
				if ok {
					if x.Sys().(syscall.WaitStatus).Signal() == syscall.SIGINT {
						return
					}
				}
				fmt.Println("💥 Application crashed:", err)
				return
			}
		}(devServer)
		devServerMu.Unlock()
	}

	// Create the goroutine to generate content.
	loadDevServer()

	// Watch the directory.
	go func() {
		fmt.Println("👀 Watching for filesystem changes")
		for {
			select {
			case event, ok := <-w.Event:
				if !ok {
					return
				}
				if strings.HasSuffix(event.Path, "/imports_gen.go") || strings.HasSuffix(event.Path, "/bin") {
					continue
				}
				fmt.Println("🗂 Filesystem changed, reloading server.")
				loadDevServer()
			case err, ok := <-w.Error:
				if !ok {
					return
				}
				panic(err)
			}
		}
	}()

	// Listen for CTRL+C.
	// The deadlock at the end is intended, this should never be used again!
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGINT)
	<-c
	devServerMu.Lock()
	if devServer != nil {
		_ = devServer.Process.Signal(syscall.SIGINT)
		os.Exit(0)
	}
}
