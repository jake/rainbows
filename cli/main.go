package main

import (
	"fmt"
	"os"
	"strings"
)

const help = `rainbows init <package name> - Used to initialise the package.
rainbows route <methods (GET/POST/PATCH/PUT/DELETE), comma separated> <route (using [type (default to string)]:name where variable)> - Create a route in your Rainbows application.
rainbows generate - Generate the Rainbows imports.
rainbows dev - Start a dev instance for Rainbows.`

func showHelp() {
	fmt.Println(help)
}

func main() {
	if len(os.Args) == 1 {
		showHelp()
		return
	}
	x := strings.ToLower(os.Args[1])
	switch x {
	case "generate":
		generate()
		return
	case "dev":
		dev()
		return
	case "init":
		if len(os.Args) == 2 {
			_ = fmt.Errorf("Not enough arguments.\n")
			os.Exit(1)
		}
		initCmd(os.Args[2])
		return
	case "route":
		if 4 > len(os.Args) {
			_ = fmt.Errorf("Not enough arguments.\n")
			os.Exit(1)
		}
		route(os.Args[2], os.Args[3])
		return
	}
	showHelp()
}
