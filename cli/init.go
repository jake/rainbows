package main

import (
	"bytes"
	"embed"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
)

//go:embed init_project
var initProject embed.FS

func initCmd(moduleName string) {
	// Generate the Go project.
	cmd := exec.Command("go", "mod", "init", moduleName)
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	if err := cmd.Run(); err != nil {
		panic(err)
	}

	// Get the Rainbows package.
	cmd = exec.Command("go", "get", "gitea.com/jake/rainbows")
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	if err := cmd.Run(); err != nil {
		panic(err)
	}

	// Get the directory entries.
	var writeDir func(string)
	writeDir = func(fp string) {
		resfp := filepath.Join("init_project", fp)
		dirEntry, err := initProject.ReadDir(resfp)
		if err != nil {
			panic(err)
		}
		for _, v := range dirEntry {
			relJoined := filepath.Join(fp, v.Name())
			if v.IsDir() {
				// Make the directory.
				if err := os.Mkdir(relJoined, 0777); err != nil {
					panic(err)
				}
				writeDir(filepath.Join(fp, v.Name()))
			} else {
				// Write the file.
				b, err := initProject.ReadFile(filepath.Join(resfp, v.Name()))
				if err != nil {
					panic(err)
				}
				if v.Name() == "imports_gen.go" {
					b = bytes.ReplaceAll(b, []byte("packagenamehere"), []byte(moduleName))
				}
				if err := ioutil.WriteFile(relJoined, b, 0700); err != nil {
					panic(err)
				}
			}
		}
	}
	writeDir(".")
}
