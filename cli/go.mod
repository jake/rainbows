module gitea.com/jake/rainbows/cli

go 1.16

require (
	github.com/dlclark/regexp2 v1.4.0
	golang.org/x/mod v0.4.1
	github.com/radovskyb/watcher v1.0.7
)
