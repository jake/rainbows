package router

import (
	"bytes"
	"github.com/valyala/fasthttp"
)

// Route is used to define the result of a HTTP route.
// ok means it was able to parse the route properly and error is route/parser errors.
type Route func(req *fasthttp.RequestCtx, args [][]byte) (ok bool, err error)

// The route without the args (handled inside the parser).
type partialRoute func(req *fasthttp.RequestCtx) (ok bool, err error)

// Defines a route stack.
type routeStack struct {
	prev  *routeStack
	route partialRoute
}

// Push to the stack.
func (r *routeStack) push(route partialRoute) {
	if r.route != nil {
		r.prev = &routeStack{prev: r.prev, route: r.route}
	}
	r.route = route
}

// Pop from the stack. Will be nil if there are no items on the stack.
func (r *routeStack) pop() partialRoute {
	route := r.route
	if r.prev == nil {
		r.route = nil
	} else {
		prev := r.prev
		r.route = prev.route
		r.prev = prev.prev
	}
	return route
}

// Define the router character.
type routerChar struct {
	// Defines the router character.
	c byte

	// Continue on from the tree.
	tree *Router
}

// Router defines the router binary tree.
type Router struct {
	// Defines if this is a dynamic fragment.
	dynamicFragment bool

	// Defines the variable routes.
	variables []*Router

	// Defines if there's any functions attached to this.
	functions []Route

	// Get the router characters.
	chars []routerChar
}

// LoadRoute loads the route into the tree.
func (r *Router) LoadRoute(routePath string, route Route) {
	// Get the tree branch.
	treeBranch := r

	// Go through the string.
	for i, v := range []byte(routePath) {
		// Ignore start slashes.
		if i == 0 && v == '/' {
			continue
		}

		// Handle variables.
		if v == ':' {
			tree := &Router{dynamicFragment: true}
			if treeBranch.variables == nil {
				treeBranch.variables = []*Router{tree}
			} else {
				treeBranch.variables = append(treeBranch.variables, tree)
			}
			treeBranch = tree
			continue
		}

		// Handle pushing the character to the appropriate place.
		found := false
		for _, x := range treeBranch.chars {
			if x.c == v {
				treeBranch = x.tree
				found = true
				break
			}
		}
		if !found {
			x := &Router{}
			treeBranch.chars = append(treeBranch.chars, routerChar{
				c:    v,
				tree: x,
			})
			treeBranch = x
		}
	}

	// Append the route.
	if treeBranch.functions == nil {
		treeBranch.functions = []Route{route}
	} else {
		treeBranch.functions = append(treeBranch.functions, route)
	}
}

// Defines a router stack.
type routerStack struct {
	prev   *routerStack
	router *Router
	args	[][]byte
	index  int
}

// Push to the stack.
func (r *routerStack) push(router *Router, index int, a [][]byte) {
	if r.router != nil {
		r.prev = &routerStack{prev: r.prev, router: r.router, index: r.index, args: a}
	}
	r.router = router
	r.index = index
	r.args = a
}

// Pop from the stack. Will be nil if there are no items on the stack.
func (r *routerStack) pop() (*Router, int, [][]byte) {
	router := r.router
	index := r.index
	args := r.args
	if r.prev == nil {
		r.router = nil
	} else {
		prev := r.prev
		r.router = prev.router
		r.index = prev.index
		r.args = prev.args
		r.prev = prev.prev
	}
	return router, index, args
}

// Go through the routers branches.
func (r *Router) goThroughBranches(priorityStackPush, stackPush func(partialRoute), path []byte, pathLen, initIndex int) {
	stack := routerStack{}
	stack.push(r, initIndex, [][]byte{})
	for r, index, args := stack.pop(); r != nil; r, index, args = stack.pop() {
		// If the index is equal to the length of the path, push everything from here and return.
		if index == pathLen {
			for _, v := range r.functions {
				if r.dynamicFragment {
					stackPush(func(req *fasthttp.RequestCtx) (ok bool, err error) { return v(req, args) })
				} else {
					priorityStackPush(func(req *fasthttp.RequestCtx) (ok bool, err error) { return v(req, args) })
				}
			}
			continue
		}

		// Get the character.
		c := path[index]

		// Handle custom pathways.
		if len(r.variables) != 0 {
			// Defines the current index.
			currentIndex := index

			// Gobble the custom argument.
			buf := bytes.Buffer{}
			for ; currentIndex < pathLen; currentIndex++ {
				b := path[currentIndex]
				if b == '/' {
					// We've hit the end of what we're supposed to parse.
					break
				}
				_ = buf.WriteByte(b)
			}
			argValue := buf.Bytes()

			// Check if the argument is valid.
			if len(argValue) != 0 {
				// Go through all of the current arguments.
				for _, v := range r.variables {
					// Go through the tree branch.
					stack.push(v, currentIndex, append(args, argValue))
				}
			}
		}

		// Get any compatible static pathways.
		for _, v := range r.chars {
			if v.c == c {
				// This is compatible. Try taking this path.
				// We do not need to handle copying args for a static path. If we get to a custom path later, that will handle it.
				stack.push(v.tree, index+1, args)
				break
			}
		}
	}
}

// Execute tries to run a route using the tree.
func (r *Router) Execute(req *fasthttp.RequestCtx) (notFound bool, err error) {
	// Get the path.
	path := req.Path()

	// Get the stacks.
	priorityStack := &routeStack{}
	stack := &routeStack{}

	// Handle if the path begins with a slash.
	index := 0
	if path[index] == '/' {
		index++
	}

	// Populate the stack.
	r.goThroughBranches(priorityStack.push, stack.push, path, len(path), index)

	// Go through all of the items in the stack.
	pop := func() partialRoute {
		if priorityStack == nil {
			// Ok, we've exhausted all priority routes.
			return stack.pop()
		}

		// Get the value.
		v := priorityStack.pop()

		// If v is nil, make priorityStack nil and pop from the other stack.
		if v == nil {
			priorityStack = nil
			return stack.pop()
		}

		// If not, return v.
		return v
	}
	for x := pop(); x != nil; x = pop() {
		ok, err := x(req)
		if err != nil {
			// Request failed.
			return false, err
		}
		if ok {
			// Request successful.
			return false, nil
		}
	}

	// Return not found.
	return true, nil
}
