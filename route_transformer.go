package rainbows

import (
	"bytes"
	"gitea.com/jake/rainbows/internal/router"
	"github.com/gabriel-vasile/mimetype"
	"github.com/valyala/fasthttp"
	"os"
	"path/filepath"
	"reflect"
	"strings"
)

// The fasthttp router which handles the low level HTTP routing.
var fasthttpRouter = router.Router{}

// Store the methods used for each request.
var methods = map[string][]string{}

// Handles serving a static file.
func handleFileServing(Path string) router.Route {
	// Pre-determine the content type of the file.
	mime, err := mimetype.DetectFile(Path)
	if err != nil {
		panic(err)
	}
	mimeStr := mime.String()
	if mimeStr == "text/plain; charset=utf-8" && strings.HasSuffix(Path, ".css") {
		mimeStr = "text/css"
	}

	// Return the route.
	return func(req *fasthttp.RequestCtx, _ [][]byte) (ok bool, err error) {
		req.Response.SetStatusCode(200)
		req.Response.Header.Set("Content-Type", mimeStr)
		f, err := os.Open(Path)
		if err != nil {
			return false, err
		}
		stat, err := f.Stat()
		if err != nil {
			return false, err
		}
		req.Response.SetBodyStream(f, int(stat.Size()))
		return true, nil
	}
}

// Methods is used to expose all the imported methods for a specific HTTP route.
func Methods(Route string) []string {
	x := methods[Route]
	if x == nil {
		x = make([]string, 0)
	}
	return x
}

// CORSRules are used to define the CORS rules.
type CORSRules struct {
	// Defines which routes are CORS.
	GET, POST, PUT, DELETE bool

	// Defines the origin which can be used.
	OriginRoute string
}

// CORSAll returns a CORSRules instance in which all methods/origins can use it.
func CORSAll() *CORSRules {
	return &CORSRules{
		GET:         true,
		POST:        true,
		PUT:         true,
		DELETE:      true,
		OriginRoute: "*",
	}
}

// Routes which have CORS rules.
var corsRoutes = map[string]*CORSRules{}

// Loads the route in the way the library expects.
func loadRoute(routePath, method string, route router.Route) {
	x := methods[routePath]
	if x == nil {
		// This is the first route! We should inject our OPTIONS handler here.
		fasthttpRouter.LoadRoute(routePath, func(req *fasthttp.RequestCtx, _ [][]byte) (ok bool, err error) {
			// Check if the route has an OPTIONS manager. Return not ok if so.
			hasOptions := false
			m := Methods(routePath)
			for _, v := range m {
				if v == "OPTIONS" {
					hasOptions = true
					break
				}
			}
			if hasOptions {
				return false, nil
			}

			// Check this is an OPTIONS request.
			if !bytes.Equal([]byte("OPTIONS"), req.Method()) {
				return false, nil
			}

			// Set the status to 204.
			req.Response.SetStatusCode(204)

			// Set the Allow header.
			req.Response.Header.Set("Allow", strings.Join(m, ", "))

			// Handle CORS rules.
			rules := corsRoutes[routePath]
			if rules != nil {
				methods := make([]string, 0, 1)
				if rules.GET {
					methods = append(methods, "GET")
				}
				if rules.POST {
					methods = append(methods, "POST")
				}
				if rules.DELETE {
					methods = append(methods, "DELETE")
				}
				if rules.PUT {
					methods = append(methods, "PUT")
				}
				req.Response.Header.Set("Access-Control-Allow-Methods", strings.Join(methods, ", "))
				req.Response.Header.Set("Access-Control-Allow-Origin", rules.OriginRoute)
			}

			// Return ok with no errors.
			return true, nil
		})
		methods[routePath] = []string{method}
	} else {
		// Append if it doesn't already exist.
		exists := false
		for _, v := range x {
			if v == method {
				exists = true
				break
			}
		}
		if !exists {
			methods[routePath] = append(x, method)
		}
	}

	// Add the route.
	methodBytes := []byte(method)
	fasthttpRouter.LoadRoute(routePath, func(req *fasthttp.RequestCtx, args [][]byte) (ok bool, err error) {
		// Check the method to make sure it is ok.
		if !bytes.Equal(methodBytes, req.Method()) {
			return false, nil
		}

		// Call the function.
		return route(req, args)
	})
}

// Check the return values and that it's a func.
func checkFuncReturn(path string, v reflect.Value) reflect.Type {
	// Ensure this is actually a function.
	if v.Kind() != reflect.Func {
		panic(path + " - expected function")
	}

	// Get the return type.
	type_ := v.Type()
	if type_.NumOut() == 2 {
		// Check the type of the returns.
		if type_.Out(0) == reflect.TypeOf((*Response)(nil)) && type_.Out(1) == reflect.TypeOf((*error)(nil)).Elem() {
			// Return the type.
			return type_
		}
	}

	// Panic about the type.
	panic(path + " - expected return of (*rainbows.Response, error)")
}

// Used to process a Go file.
func processGoFile(httpRoute, path string) {
	// Process CORS rules.
	if rule := loadedRules[path]; rule != nil {
		corsRoutes[httpRoute] = rule
	}

	// Get the methods.
	if methods := loadedFiles[path]; methods != nil {
		// Go through the routes.
		for _, v := range methods {
			// Do a basic sanity check of the functions. This basically consists of checking:
			// 1) The return type for both middleware and the handler are (*rainbows.Response, error).
			// 2) The starting arguments are rainbows.Request for the handler and rainbows.Request, *rainbows.FunctionCall for middleware.
			reflectHandler := reflect.ValueOf(v.function)
			type_ := checkFuncReturn(path, reflectHandler)
			if 1 > type_.NumIn() {
				// We expect at least rainbows.Request here.
				panic(path + " - at least rainbows.Request is expected here for arguments")
			}
			firstArg := type_.In(0)
			if firstArg != reflect.TypeOf((*Request)(nil)).Elem() {
				panic(path + " - expected rainbows.Request for the first argument, got " + firstArg.String())
			}
			reflectMiddleware := make([]reflect.Value, len(v.middleware))
			for i, v := range v.middleware {
				// Get the reflect value.
				reflectValue := reflect.ValueOf(v)

				// Check the return/input types and that it is a function.
				type_ := checkFuncReturn(path, reflectValue)
				if 2 > type_.NumIn() {
					// We expect at least rainbows.Request and *rainbows.FunctionCall here.
					panic(path + " - at least rainbows.Request and *rainbows.FunctionCall is expected here for arguments")
				}
				firstArg := type_.In(0)
				if firstArg != reflect.TypeOf((Request)(nil)) {
					panic(path + " - expected rainbows.Request for the first argument, got " + firstArg.String())
				}
				secondArg := type_.In(1)
				if secondArg != reflect.TypeOf((*FunctionCall)(nil)) {
					panic(path + " - expected *rainbows.FunctionCall for the second argument, got " + secondArg.String())
				}

				// Set the reflect value in the array.
				reflectMiddleware[i] = reflectValue
			}

			// Insert the execution function.
			loadRoute(httpRoute, v.method, func(fasthttpReq *fasthttp.RequestCtx, args [][]byte) (bool, error) {
				// Create the function call object.
				f := FunctionCall{
					requestArgs:    args,
					handler:        reflectHandler,
					middleware:     reflectMiddleware,
					additionalArgs: []interface{}{},
					req:            newFasthttpRequest(fasthttpReq),
				}

				// Call it for the response.
				resp, err := f.Run()
				if err == badTransform {
					return false, nil
				}
				if err != nil {
					return false, err
				}

				// Write the response into the fasthttp handler and return ok.
				resp.writeIntoFasthttp(fasthttpReq)
				return true, nil
			})
		}

		// Return here so we don't accidentally panic.
		return
	}

	// Panic about the file not being used.
	panic(path + " - This go file does not register any routes to Rainbows. Either move it out of the route structure or use \"rainbows generate\" to generate a route file to include it.")
}

// Root is used to define a root folder which Rainbows will use.
func Root(Folder string) {
	// Get the absolute path.
	abs, err := filepath.Abs(Folder)
	if err != nil {
		// Unable to translate the path.
		panic(err)
	}

	// Go through each item in the path.
	if err := filepath.Walk(abs, func(path string, info os.FileInfo, err error) error {
		// Pass through errors.
		if err != nil {
			return err
		}

		// Transform the path into a route.
		pathFragment := path[len(abs):]
		split := strings.Split(pathFragment, string(filepath.Separator))
		initHttpRoute := "/"
		indexLen := 0
		for _, v := range split {
			if v != "" {
				if v == "param" || v == "param.go" || strings.HasPrefix(v, "param_") {
					// This is a param item.
					v = ":"
				} else {
					// Check if this is an index.
					if strings.HasPrefix(v, "index") {
						indexLen = len(v)
					} else {
						indexLen = 0
					}
				}
				initHttpRoute += v + "/"
			}
		}
		sub := 1
		if strings.HasSuffix(initHttpRoute, ".go") {
			sub += 3
			if indexLen != 0 {
				indexLen -= 3
			}
		}
		initHttpRoute = initHttpRoute[:len(initHttpRoute)-sub]

		// Defines thr HTTP routes.
		httpRoutes := []string{
			initHttpRoute,
		}
		if indexLen != 0 {
			// This is also a index route.
			httpRoutes = append(httpRoutes, initHttpRoute[:len(initHttpRoute)-indexLen-1])
		}

		// Check if this is a folder.
		if !info.IsDir() {
			if strings.HasSuffix(info.Name(), ".go") {
				// Since this is a .go file, we should handle trying to load it.
				// If we don't have any routes relating to the folder it is in, we should throw an error.
				for _, v := range httpRoutes {
					processGoFile(v, path)
				}
			} else {
				// Handle GET requests for this file. It is a static asset.
				for _, v := range httpRoutes {
					loadRoute(v, "GET", handleFileServing(path))
				}
			}
		}

		// Return no errors.
		return nil
	}); err != nil {
		panic(err)
	}
}
