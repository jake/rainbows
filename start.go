package rainbows

import (
	"fmt"
	"github.com/valyala/fasthttp"
)

// Start is used to launch the application.
func Start(Addr string) {
	loadedFiles = nil
	loadedRules = nil
	fmt.Println("🌈 Server starting on " + Addr)
	if err := fasthttp.ListenAndServe(Addr, handleRequest); err != nil {
		panic(err)
	}
}
