package rainbows

import (
	"fmt"
	"github.com/valyala/fasthttp"
	"time"
)

// Handles safely running the handler.
func safelyRunHandler(req *fasthttp.RequestCtx) (err error) {
	// Handle panics.
	defer func() {
		if r := recover(); r != nil {
			err = r.(error)
		}
	}()

	// Process the route.
	notFound, err := fasthttpRouter.Execute(req)
	if err != nil {
		return err
	}
	if notFound {
		return ErrNotFound
	}
	return nil
}

// Handles the fasthttp request.
func handleRequest(req *fasthttp.RequestCtx) {
	// Handle trailing slashes.
	p := req.Path()
	x := len(p)
	if x > 1 {
		if p[x-1] == '/' {
			req.Redirect(string(p[:x-1]), 301)
			return
		}
	}

	// Run the request.
	if err := safelyRunHandler(req); err != nil {
		if resp := ErrorHandler(newFasthttpRequest(req), err); resp != nil {
			resp.writeIntoFasthttp(req)
		}
	}
	path := string(req.Path())
	go func() {
		// Get the request duration.
		d := time.Now().Sub(req.Time())

		// Log the request.
		fmt.Println("💻", path, d)
	}()
}
