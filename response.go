package rainbows

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"github.com/fasthttp/websocket"
	"github.com/valyala/fasthttp"
	"io"
	"io/ioutil"
	"strconv"
	"strings"
)

// Response is the response type which is sent back.
type Response struct {
	// WebsocketHandler is defined if the result is a WebSocket.
	WebsocketHandler func(*websocket.Conn) error

	// StatusCode is used to define the status code which should be sent back.
	StatusCode int

	// Reader is used to define the reader which should be sent back.
	Reader io.ReadCloser

	// Headers are the headers which should be sent back with the response.
	Headers map[string][]byte
}

// Handles upgrading to WebSocket's.
var upgrader = websocket.FastHTTPUpgrader{}

// Writes the response into a fasthttp request context.
func (r Response) writeIntoFasthttp(ctx *fasthttp.RequestCtx) {
	if r.WebsocketHandler != nil {
		// In the event of a WebSocket handler, we should try and upgrade the connection.
		_ = upgrader.Upgrade(ctx, func(conn *websocket.Conn) {
			if err := r.WebsocketHandler(conn); err != nil {
				_ = conn.Close()
				ErrorHandler(newFasthttpRequest(ctx), err)
			}
		})
		return
	}

	ctx.Response.SetStatusCode(r.StatusCode)
	for k, v := range r.Headers {
		ctx.Response.Header.SetBytesV(k, v)
	}
	if r.Reader != nil {
		ctx.Response.SetBodyStream(r.Reader, -1)
	}
}

// Text is used to write a text response.
func Text(Text string, StatusCode int) (*Response, error) {
	r := strings.NewReader(Text)
	return &Response{
		StatusCode: StatusCode,
		Reader:     ioutil.NopCloser(r),
		Headers: map[string][]byte{
			"Content-Type": []byte("text/plain"),
			"Content-Length": []byte(strconv.Itoa(r.Len())),
		},
	}, nil
}

// HTML is used to write a HTML response.
func HTML(HTMLContent string, StatusCode int) (*Response, error) {
	r := strings.NewReader(HTMLContent)
	return &Response{
		StatusCode: StatusCode,
		Reader:     ioutil.NopCloser(r),
		Headers: map[string][]byte{
			"Content-Type": []byte("application/html"),
			"Content-Length": []byte(strconv.Itoa(r.Len())),
		},
	}, nil
}

// JSON is used to write a JSON response.
func JSON(content interface{}, StatusCode int) (*Response, error) {
	b, err := json.Marshal(content)
	if err != nil {
		return nil, err
	}
	return &Response{
		StatusCode: StatusCode,
		Reader:     ioutil.NopCloser(bytes.NewReader(b)),
		Headers: map[string][]byte{
			"Content-Type": []byte("application/json"),
			"Content-Length": []byte(strconv.Itoa(len(b))),
		},
	}, nil
}

// XML is used to write a XML response.
func XML(content interface{}, StatusCode int) (*Response, error) {
	b, err := xml.Marshal(content)
	if err != nil {
		return nil, err
	}
	return &Response{
		StatusCode: StatusCode,
		Reader:     ioutil.NopCloser(bytes.NewReader(b)),
		Headers: map[string][]byte{
			"Content-Type": []byte("application/xml"),
			"Content-Length": []byte(strconv.Itoa(len(b))),
		},
	}, nil
}

// WebSocket is used to respond with a WebSocket stream if possible.
// Note that if there was an error with the socket, it will be put through the error handler but the error response will not be sent.
func WebSocket(handler func(conn *websocket.Conn) error) (*Response, error) {
	return &Response{WebsocketHandler: handler}, nil
}

// NoContent returns a 204.
func NoContent() (*Response, error){
	return &Response{StatusCode: 204}, nil
}
