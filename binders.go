package rainbows

import "encoding/json"

var binders = map[string]func([]byte, interface{}) error{
	// A blank string means the user hasn't provided a content type.
	// By default, Rainbows will take this as it should be JSON.
	"": json.Unmarshal,
}

// AddBinder allows you to set a binder for a request with a custom content type.
// A blank content type will mean that this will be the default binder if the user does not specify the content type.
func AddBinder(ContentType string, Binder func([]byte, interface{}) error) {
	binders[ContentType] = Binder
}
