package rainbows

import "runtime"

var (
	loadedFiles = map[string][]*loadedFunc{}
	loadedRules = map[string]*CORSRules{}
)

type loadedFunc struct {
	function   interface{}
	middleware []interface{}
	method     string
}

func register(method string, function interface{}, middleware []interface{}) {
	_, fp, _, ok := runtime.Caller(2)
	if !ok {
		panic("unable to get file path of caller!")
	}
	x := loadedFiles[fp]
	if x == nil {
		x = []*loadedFunc{{
			function:   function,
			middleware: middleware,
			method:     method,
		}}
	} else {
		x = append(x, &loadedFunc{
			function:   function,
			middleware: middleware,
			method:     method,
		})
	}
	loadedFiles[fp] = x
}

// SetCORSRules is used to configure CORS rules for a specific path.
func SetCORSRules(rules *CORSRules) {
	_, fp, _, ok := runtime.Caller(1)
	if !ok {
		panic("unable to get file path of caller!")
	}
	loadedRules[fp] = rules
}

// GET is used to register a HTTP GET route.
func GET(function interface{}, middleware ...interface{}) {
	register("GET", function, middleware)
}

// POST is used to register a HTTP POST route.
func POST(function interface{}, middleware ...interface{}) {
	register("POST", function, middleware)
}

// PATCH is used to register a HTTP PATCH route.
func PATCH(function interface{}, middleware ...interface{}) {
	register("PATCH", function, middleware)
}

// PUT is used to register a HTTP PUT route.
func PUT(function interface{}, middleware ...interface{}) {
	register("PUT", function, middleware)
}

// DELETE is used to register a HTTP DELETE route.
func DELETE(function interface{}, middleware ...interface{}) {
	register("DELETE", function, middleware)
}
