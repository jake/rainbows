package rainbows

import "fmt"

// ErrorHandlerFunc is the function signature used to handle errors.
type ErrorHandlerFunc func(req Request, err error) *Response

// ErrorHandler is the function which is called on error.
var ErrorHandler ErrorHandlerFunc = func(req Request, err error) *Response {
	if err == ErrNotFound {
		r, _ := Text("Route not found.", 404)
		return r
	}
	_ = fmt.Errorf("❌ %s", err)
	r, _ := Text("Internal Server Error", 500)
	return r
}
